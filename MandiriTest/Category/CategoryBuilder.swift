//
//  CategoryBuilder.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 07/08/23.
//

import Foundation
import UIKit

class CategoryBuilder: BuildModuleProtocol {
    static func build() -> UINavigationController {
        let viewController: CategoryViewController = CategoryViewController()
        let presenter = CategoryPresenter()
        let interactor = CategoryInteractor()
        let router = CatagoryRouter(viewController: viewController)
        
        viewController.presenter = presenter
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        
        return UINavigationController(rootViewController: viewController)
    }
}
