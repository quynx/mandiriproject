//
//  CategoryInteractor.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 08/08/23.
//

import Foundation
import UIKit

protocol CategoryInteractorProtocol: AnyObject {
    func initData()
    func itemCount() -> Int
    func categoryItem(at indexPath: IndexPath) -> CategoryItem
    func pushToSourceList(at indexPath: IndexPath)
}

class CategoryInteractor: CategoryInteractorProtocol {
    
    var items = [
        CategoryItem(name: "business", color: UIColor.systemBlue),
        CategoryItem(name: "entertainment", color: UIColor.systemRed),
        CategoryItem(name: "general", color: UIColor.systemCyan),
        CategoryItem(name: "health", color: UIColor.systemMint),
        CategoryItem(name: "science", color: UIColor.systemGreen),
        CategoryItem(name: "sport", color: UIColor.systemGray),
        CategoryItem(name: "technology", color: UIColor.systemTeal)
    ]
    
    weak var presenter: CategoryPresenterOutput!
}

extension CategoryInteractor {
    
    func initData() {
        presenter.presentCategory(item: items)
    }
    
    func itemCount() -> Int {
        return items.count
    }
    
    func categoryItem(at indexPath: IndexPath) -> CategoryItem {
        return items[indexPath.row]
    }
    
    func pushToSourceList(at indexPath: IndexPath) {
        let category = items[indexPath.row]
        presenter.presentToListSource(category)
    }
}
