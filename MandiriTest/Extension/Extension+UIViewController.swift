//
//  Extension+UIViewController.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 10/08/23.
//

import Foundation
import UIKit

extension UIViewController {
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default)
        
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}
