//
//  extension+UIImageView.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 10/08/23.
//

import Foundation
import UIKit

extension UIImageView {
    func load(from urlString: String) {
        if let url = URL(string: urlString) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
            
            task.resume()
        }
    }
}

