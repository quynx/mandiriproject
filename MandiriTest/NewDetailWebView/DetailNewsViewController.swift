//
//  DetailNewsViewController.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 04/08/23.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
    
    var article: Article!
    
    let webview = WKWebView()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func loadView() {
        view = webview
        view.showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.webview.load(self.article.url)
            self.view.hideLoading()
        }
    }
}

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
