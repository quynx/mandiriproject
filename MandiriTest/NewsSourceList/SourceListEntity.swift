//
//  SourceListEntity.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 4/08/23.
//

import Foundation

struct SourceResponse: Codable {
    var sources: [Source]
}

// MARK: - Source
struct Source: Codable {
    var id, name, description: String
    var url: String
    var category, language, country: String
}
