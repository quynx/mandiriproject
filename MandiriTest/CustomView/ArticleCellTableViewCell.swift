//
//  ArticleCellTableViewCell.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 10/08/23.
//

import UIKit

class ArticleCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(article: Article) {
        articleImageView.load(from: article.urlToImage ?? "")
        titleLabel.text = article.title
        contentLabel.text = article.description?.replacingOccurrences(of: "\"", with: "") ?? ""
        sourceLabel.text = article.source.name
    }
    
}
