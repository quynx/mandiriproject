//
//  CategoryTableViewCell.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 08/08/23.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var label: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bodyView.layer.cornerRadius = 12
        bodyView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(category: CategoryItem) {
        label.text = category.name?.capitalized
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 17)
        if let color = category.color {
            bodyView.backgroundColor = color
            bodyView.alpha = 1
        }
    }
    
}
